import React, { Component } from 'react';
import logo from './logo.png';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Table } from 'reactstrap';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      startDate: null,
      isLoading: false,
      flights: []
    };
    this.handleChange = this.handleChange.bind(this);
  }
  
  handleChange(date) {
    if (date !== null) {
      this.setState({
        startDate: date,
        isLoading: true
      },
      () => {
        let dStr = this.state.startDate.format("DD/MM/YYYY");
        let that = this;
        fetch('/flights?date=' + dStr)
          .then(function(response) {
            return response.json();
          })
          .then(function(myJson) {
            that.setState({
              flights: myJson,
              isLoading: false
            });
          });
        });
    } else {
      this.setState({
        startDate: date,
        flights: []
      });
    }
  }
  
  render() {
    
    const flightTableContent = this.state.flights.map((flight, index) => {
        return <tr key={index}>
          <td>{index + 1}</td>
          <td>{flight.iata}</td>
          <td>{flight.destination}</td>
          <td>{flight.number}</td>
          <td>{flight.time}</td>
        </tr>
    });
    
    if (this.state.isLoading) {
        return <div class="loading"><p>Loading...</p></div>;
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Flight Information</h1>
        </header>
        <div className="App-intro">
          <h2>Retrieve Flight Information</h2>
          <DatePicker
            selected={this.state.startDate}
            onChange={this.handleChange}
            dateFormat="DD/MM/YYYY"
            isClearable={true}
            showYearDropdown
            placeholderText="Select a date"
          />
          <Container>
            <Table>
              <thead>
                <tr>
                  <th width="5%">#</th>
                  <th width="5%">Iata</th>
                  <th width="20%">Destination</th>
                  <th width="20%">Flight Number</th>
                  <th width="10%">Time</th>
                </tr>
              </thead>
              <tbody>
                {flightTableContent}
              </tbody>
            </Table>
            </Container>
        </div>
      </div>
    );
  }
}

export default App;
