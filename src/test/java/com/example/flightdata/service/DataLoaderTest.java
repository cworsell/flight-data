package com.example.flightdata.service;

import com.example.flightdata.FlightDataApplication;
import com.example.flightdata.model.Flight;
import java.util.List;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author craig.worsell
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDataApplication.class)
public class DataLoaderTest {
    
    @Autowired
    private DataLoader dataLoader;
    
    @Test
    public void loadDataFromCsv_Test() {
        List<Flight> flights = dataLoader.loadCsvData();
        assertFalse(flights.isEmpty());
        assertTrue(flights.size() == 27);
    }
}
