package com.example.flightdata.controller;

import com.example.flightdata.FlightDataApplication;
import com.example.flightdata.model.Flight;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author craig.worsell
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightDataApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FlightsControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;
    
    private final ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .build();
    }
    
    @Test
    public void getFlights_Test() throws Exception {
        ResultActions result
                = mockMvc.perform(get("/flights"))
                        .andExpect(status().isOk());
        String resultString = result.andReturn().getResponse().getContentAsString();
        List<Flight> list = mapper.readValue(resultString, new TypeReference<List<Flight>>() { });
        assertNotNull("List of flights should not be null", list);
        assertFalse("List of flights should not be empty", list.isEmpty());
    }
    
    @Test
    public void getFlighsWithDate_Test() throws Exception {
        ResultActions result
                = mockMvc.perform(get("/flights?date=01/08/2018"))
                        .andExpect(status().isOk());
        String resultString = result.andReturn().getResponse().getContentAsString();
        List<Flight> list = mapper.readValue(resultString, new TypeReference<List<Flight>>() { });
        assertNotNull("List of flights should not be null", list);
        assertFalse("List of flights should not be empty", list.isEmpty());
        assertTrue("List should be less than 27", list.size() < 27);
        List<Flight> sortedList = list.stream().collect(Collectors.toList());
        Collections.sort(sortedList, (f1, f2) -> f1.getLocalTime().compareTo(f2.getLocalTime()));
        assertArrayEquals("List should equal list after copying and sorting", list.toArray(), sortedList.toArray());
    }
}
