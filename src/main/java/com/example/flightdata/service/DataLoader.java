package com.example.flightdata.service;

import com.example.flightdata.model.Flight;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

/**
 *
 * @author craig.worsell
 */
@Service
public class DataLoader {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DataLoader.class);
    
    private final String COMMA = ",";
    
    public List<Flight> loadCsvData() {
        List<Flight> flights = new ArrayList<>();
       
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(
                        DataLoader.class.getResourceAsStream("/flights.csv")))) {
//                new FileInputStream(
//                    new ClassPathResource("flights.csv").getFile())))) {
            
            // skip the header of the csv
            flights = br.lines().skip(1).map(lineToObject).collect(Collectors.toList());
            
        } catch(IOException e) {
            LOGGER.warn("Data could not be loaded", e);
        }
        return flights;
    }
    
    private final Function<String, Flight> lineToObject = (line) -> {
        String[] p = line.split(COMMA);
        Flight object = new Flight();
        object.setTime(p[0]);
        object.setDestination(p[1]);
        object.setIata(p[2]);
        object.setNumber(p[3]);
        try {
            object.setSunday(dayAvailable(p[4]));
            object.setMonday(dayAvailable(p[5]));
            object.setTuesday(dayAvailable(p[6]));
            object.setWednesday(dayAvailable(p[7]));
            object.setThursday(dayAvailable(p[8]));
            object.setFriday(dayAvailable(p[9]));
            object.setSaturday(dayAvailable(p[10]));
        } catch (IndexOutOfBoundsException e) {
            // do nothing
            // Empty fields at the end of the row are not added to the array
        }
        return object;
    };
            
    private boolean dayAvailable(String x) {
        return "x".equals(x);
    }
}
