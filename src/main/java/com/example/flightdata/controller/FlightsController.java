package com.example.flightdata.controller;

import com.example.flightdata.model.Flight;
import com.example.flightdata.service.DataLoader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author craig.worsell
 */

@RestController
public class FlightsController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FlightsController.class);
    
    @Autowired
    private DataLoader dataLoader;
    
    @GetMapping("flights")
    public List<Flight> getFlights(@RequestParam(value="date", required=false) String date) {
        List<Flight> flights = dataLoader.loadCsvData();
        if (date != null) {
            List<Flight> filtered = filterFlights(flights, date);
            Collections.sort(filtered, (f1, f2) -> f1.getLocalTime().compareTo(f2.getLocalTime()));
            return filtered;
        } else {
            return flights;
        }        
    }
    
    private List<Flight> filterFlights(List<Flight> flights, String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate dt = LocalDate.parse(date, formatter);
        String sDow = dt.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.UK);
        List<Flight> filtered = flights.stream()
                .filter(flight -> {
                    try {
                        Class clz = flight.getClass();
                        Method m = clz.getMethod("is" + sDow);
                        return (Boolean) m.invoke(flight);
                    } catch (IllegalAccessException | IllegalArgumentException 
                            | NoSuchMethodException | SecurityException | InvocationTargetException ex) {
                        // do nothing
                        return false;
                    }
                })
                .collect(Collectors.toList());
        return filtered;
    }
}
