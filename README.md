# Flight Data Application

The application is built using Spring Boot, Maven and React for the front end. 
There is a Maven plugin to bundle the front end and back end together however this would be kept separate in a live environment
but it keeps it easier to build and run this test. Junit has been used for testing.

## To Build and Run
 - Clone the repository from git clone https://cworsell@bitbucket.org/cworsell/flight-data.git
 - Navigate to the main flight-data folder
 - To build use `mvn -Pprod clean install`
 - To run the created jar use `java -jar flight-data-0.0.1-SNAPSHOT.jar`
 - Use a browser to navigate to localhost:8080 to reach the front end ui. 